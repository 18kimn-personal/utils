#! /bin/sh

# Script to automate some manual steps for installing AppImage files
# for the local user 

# usage:
# ./installer.sh <path/to/appimage>

# Overview
# - grab filename variables
# - make temp file
# - get assets from appimage
# - install step: move assets to correct places
# - delete temp directory

with_extension=$(basename $1)
app_basename=$(basename $1 .AppImage)

# move to temp file
mkdir -p "/tmp/app-install/$app_basename"
cp $1 "/tmp/app-install/$app_basename"
cd "/tmp/app-install/$app_basename"

# extract the appimage so that we can get the desktop file
# and do it quietly (-quiet doesnt make desktop file for some reason)
chmod +x $with_extension
"./$with_extension" --appimage-extract > /dev/null 2>&1 
cd squashfs-root

# extract program name from desktop file (ls *desktop doesnt work for some reason)
desktop_filename=$(ls . | grep -o *.desktop)
exec_line=$(cat $desktop_filename | grep Exec= | head -n 1)
program_name=$(echo $exec_line | grep -oP '(?<=Exec=)[A-Za-z0-9]*') 

# replace image name and exec in the desktop file with the program name
image_filename=$(ls . | grep -o *png | head -n 1)
cat $desktop_filename | sed -e "s/Icon=.*/Icon=\/home\/nathan\/.local\/share\/icons\/$program_name.png/" > temp.desktop
mv temp.desktop $desktop_filename
cat $desktop_filename | sed -e "s/Exec=.*/Exec=\/home\/nathan\/.local\/bin\/$program_name/" > temp.desktop
mv temp.desktop $desktop_filename

# Installation step -- moving files to correct places
# 1. desktop file so that it shows up in my launcher
cp $desktop_filename /home/nathan/.local/share/applications/$program_name.desktop
# 2. image file which should be stored in ~/.local/share/icons
mv $image_filename /home/nathan/.local/share/icons/$program_name.png
# 3. actual executable, with the program name the desktop file specifies
cd ..
mv $with_extension /home/nathan/.local/bin/$program_name
# 4. assets for the appimage, just to keep them on hand
mv squashfs-root /home/nathan/.local/share/$program_name

rm -rf "/tmp/app-install/$app_basename"

echo "$program_name installed!"
