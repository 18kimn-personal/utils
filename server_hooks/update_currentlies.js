import { promises as fs } from "fs";
import readline from "readline/promises";
import { stdin, stdout } from "process";
import yaml from "yaml";

const PORT = 7000;

/*
 * pull random currently listening song via spotify and fetch
 * - save with preview link
 * ask for book title, author, link
 *
 * save in YAML
 * sh script will take care of ssh-ing it over
 */

async function main() {
  const token = await fetch("http://localhost:" + PORT + "/token").then((res) =>
    res.text()
  );

  const { items } = await fetch(
    "https://api.spotify.com/v1/me/player/recently-played?limit=50",
    { headers: { Authorization: "Bearer " + token } }
  ).then((res) => res.json());

  const { track } = items[Math.floor(Math.random() * items.length)];
  const singer = track.artists[0].name;
  const url = track.external_urls.spotify;
  const { name, preview_url } = track;
  const listening = { singer, title: name, link: url, preview_url };

  const rl = readline.createInterface({ input: stdin, output: stdout });

  const title = await rl.question("reading: ");
  const author = await rl.question("by: ");
  const link = await rl.question("link: ");
  const thinking = await rl.question("and thinking about: ");
  rl.close();

  const currently = {
    listening,
    reading: { title, author, link },
    thinking,
  };

  fs.writeFile(
    "/tmp/currently.yaml",

    yaml.stringify(currently)
  );
}

main();
