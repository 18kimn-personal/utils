#! /usr/bin/env zsh
cd "$(dirname "$0")"

current_time=$(date +%H:%M)

if [[ "$current_time" > "21:00" ]] || [[ "$current_time" < "07:00" ]];
then
  BAT_THEME='Monokai Extended Bright'
  sedpat='2s/false/true/'
  mode="mirage"
else
  BAT_THEME='Monokai Extended Light'
  sedpat='2s/true/false/'
  mode="light"
fi

echo "let ayucolor=\"$mode\"" > /home/nathan/.config/nvim/day-night.vim

gtk=$(cat /home/nathan/.config/gtk-3.0/settings.ini | sed $sedpat)
echo $gtk > /home/nathan/.config/gtk-3.0/settings.ini
echo $gtk > /home/nathan/.config/gtk-4.0/settings.ini

bat_string="#! /bin/zsh\n\nexport BAT_THEME='$BAT_THEME'"
echo $bat_string > ./bat_theme

cat /home/nathan/.config/gtk-3.0/settings.ini | sed '2s/false/true/'

cp "./$mode.Xresources" "/home/nathan/.Xresources"
xrdb -merge ~/.Xresources
