# utils

A collection of personal tools I use to manage my computer. Some are just for
fun, some make workflow more fun, some make working safer or easier. All are
just brief ideas I had and wrote up quite quickly; please don't look too hard.
