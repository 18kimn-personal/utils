import { promises as fs } from "fs"
import { resolve } from "path"
import { load } from "js-yaml"

const ZETTEL_DIR = "/home/nathan/main/personal/thoughts"
const TAGS_PATH = "/home/nathan/.vim/words/markdown.txt"

/** reads my zettelkasten notes and generates
 * a list of tags that vim can use for autocompletion
 * */
async function genTags() {
  const files = await fs.readdir(ZETTEL_DIR, { withFileTypes: true })
  const tags = await files.reduce(async (accPromise, file) => {
    const acc = await accPromise
    if (file.isDirectory()) return acc
    const text = await fs.readFile(
      resolve(ZETTEL_DIR, file.name),
      "utf-8"
    )
    const frontmatter = text.split("---\n")[1]
    if (!frontmatter) return acc
    const { tags } = load(frontmatter)
    if (!tags) return acc
    tags.forEach((tag) => {
      !acc.includes(tag) && acc.push(tag)
    })
    return acc
  }, Promise.resolve([]))

  fs.writeFile(TAGS_PATH, tags.join("\n"))
}

genTags()
