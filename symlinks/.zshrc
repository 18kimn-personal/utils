# zsh essentials --------------
export ZSH="/usr/share/oh-my-zsh/"
ZSH_THEME="robbyrussell"
plugins=(git fzf)
source $ZSH/oh-my-zsh.sh

# modified the prompt since my urxvt doesn't support OMZ symbols
autoload -Uz vcs_info # enable vcs_info # what the hell is that
precmd () { vcs_info } # always load before displaying the prompt
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' unstagedstr ' *'
zstyle ':vcs_info:*' stagedstr ' +'
zstyle ':vcs_info:*' formats ' (%F{red}%b%c%u%f)'
location=' %F{blue}%~%f'
PROMPT='%B$location$vcs_info_msg_0_: %b'
# no funny icons, just dir in blue, git branch in red, git changes with +*

# --- useful to open shells in same directory (via an i3 command)
updatedir() { eval "pwd > /tmp/whereami" }
precmd_functions=(updatedir)

# environment variables -----------------
export EDITOR="nvim"
export ARCHFLAGS="-arch x86_64"
if type rg &> /dev/null; then # for zsh
  export FZF_DEFAULT_COMMAND='ag -i'
  export FZF_DEFAULT_OPTS='-m --height 50% --border'
fi
export m="$HOME/main"
export p="$m/personal"
export s="$m/school"
export t="$s/imf_loans/content/text"
export FZF_DEFAULT_COMMAND='ag --hidden --ignore=static -g ""'
export VI_MODE_SET_CURSOR=true
export VI_MODE_RESET_PROMPT_ON_MODE_CHANGE=true
export MODE_INDICATOR="%F{white}(normal)%f"
# bat theme env var is updated via a cron job
export WINEPREFIX=~/.wine32
export GIT_PAGER=cat
export PNPM_HOME="/home/nathan/.local/share/pnpm"

# path modifications ---------
newpaths=(
"$HOME/.yarn/bin"		
"$HOME/bin"
"/usr/local/bin"
"$HOME/main/personal/utils/bin"
"/opt/cisco/anyconnect/bin"
"$HOME/.local/bin"
"$HOME/.cargo/bin"
"$PNPM_HOME"
)

for newpath in $newpaths
do
  PATH="$PATH:$newpath"
done
export PATH

# and aliases --------------
alias oj='journal'
alias yvpn='vpn -s connect access.yale.edu'
alias cdr='cd $(git rev-parse --show-toplevel)'
alias cdn='cd ~/main/personal/thoughts'
alias v='nvim'
alias vim='nvim'
alias psid='nohup remmina ~/main/work/zang/cpub-SRC_Remote_Users-SRC_Remote_Users-CmsTempVm.rdp >/dev/null 2>&1 &'
alias fnv='nvim $(fzf)'
alias sde='sudoedit'
alias up='sudo pacman -Syu'
alias e='exit'
alias ka='killall --quiet'
alias iw='iwctl station wlan0'
alias f='ranger'

open-vault(){
if [[ $(findmnt -M ~/main/personal/decrypted) ]]; then
  echo "Vault already opened."
else 
  encfs ~/main/personal/private ~/main/personal/decrypted
fi
}
alias close-vault='fusermount -u ~/main/personal/decrypted'

# for xrandr / monitor configs 
correct-res(){
  xrandr --newmode "1600-900"  118.25  1600 1696 1856 2112  900 903 908 934 -hsync +vsync 
  xrandr --addmode eDP-1 1600-900
  xrandr --output eDP-1 --mode 1600-900
}

# various setup scripts
source "$p/utils/day-night/bat_theme" # dynamic theme loading
#

# random other stuff ------
setxkbmap -option caps:escape
echo -e -n "\x1b[\x35 q"
~/main/personal/utils/day-night/update.sh > /dev/null
xrdb -merge ~/.Xresources # force update of theming
bindkey -v

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  --no-use # This loads nvm

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/opt/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/opt/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/opt/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

# if been more than three days, tell me to update packages
startup_message(){
  last_update=$(cat ~/.last-update | bc)
  current_time=$(date +%s)
  ((diff=current_time - last_update))

  if [ $diff -gt  259200 ]; then
     vared -p "It's been more than three days since the last system update. 
Should I update now? " -c should_update
     if [ $should_update = "y" ]; then
       echo "updating now :)"
       yay -Syu
     else
       echo "ok, I'll update later"
       echo $(date +%s) > ~/.last-update
     fi
  fi
}


startup_message
