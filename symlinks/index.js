import os from "os"
import path from "path"
import { exec } from "child_process"
import { fileURLToPath } from "url"
import { promises as fs } from "fs"
const __dirname = path.dirname(fileURLToPath(import.meta.url))
const home = os.homedir()

/**
 * tiny function to symlink config files here to their appropriate locations
 * and yeah im doing this in Node instead of bash/zsh/sh LOL because
 * the symlink locations are stored in the index.json file
 */
function make_symlink({ basename, target_location }) {
  const full_path = path.resolve(
    home,
    typeof target_location === "undefined" ? "" : target_location
  )
  exec(`ln -sf ${__dirname}/${basename} ${full_path}`)
}

const index = await fs.readFile(
  path.resolve(__dirname, "index.json"),
  "utf-8"
)
JSON.parse(index).forEach((entry) => make_symlink(entry))
