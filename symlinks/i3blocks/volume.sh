#!/usr/bin/env zsh
volume=$(pactl get-sink-volume @DEFAULT_SINK@ | \
  head -n 1 | \
  awk '{print $5}'
)

muted=$(pactl get-sink-mute @DEFAULT_SINK@ | awk '{print $2}')

if [[ $muted == "yes" ]]
then
  echo " 🔇 $volume  "
else
  echo " 🕪 $volume  "
fi

