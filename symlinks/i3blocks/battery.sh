#! /usr/bin/env zsh
info=$(upower -e | grep battery | xargs upower -i)

charge=$(echo $info | grep percentage | awk '{printf("%5s \n", $2)}')

state=$(echo $info | grep state | awk '{printf("%5s \n", $2)}')
# "fully-charged" -> "charged"
state=$(echo "${state/fully-/}")

# pipe into xargs = trim string
time=$(echo $info | grep time | awk -F ':' '{printf $2}' | xargs)
# 30 minutes left -> 30m left
time=$(echo "(${time/ minutes/m} left)  ")
time=$(echo "${time/ hours/h}")

if [[ $state = "charged " ]]
then
  echo " 🔋$charge $state  "
else
  echo " 🔋$charge $state$time"
fi
