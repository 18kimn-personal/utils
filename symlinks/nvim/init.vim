call plug#begin('~/.vim/plugged')
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'dense-analysis/ale' 
  Plug 'sirver/ultisnips'
  Plug 'evanleck/vim-svelte'  
  Plug 'rust-lang/rust.vim'
  Plug 'MaxMEllon/vim-jsx-pretty'

  Plug 'tpope/vim-vinegar'
  Plug 'ap/vim-css-color'
  Plug 'junegunn/fzf.vim'
  Plug 'jalvesaq/zotcite'

  Plug 'racer-rust/vim-racer'

call plug#end()

" sets
set nonumber
set nocp
set title
" set mouse=a " commented this 2022-02-22 just for practicing
set tw=70
" tw = 'text width'
set cursorline
set noswapfile
" 'better' tabs
set smarttab
set tabstop=2
set shiftwidth=2
set expandtab
set foldmethod=syntax
set nofoldenable
set rtp+=/usr/bin/fzf
" ngl i completely frogot what this shit does 
set suffixesadd=.md
set clipboard=unnamedplus
set wildmode=longest:full,full
set autoread
au FocusGained * :checktime

set statusline=\ %f%=
set statusline+=%l/%L

" hard-coding node path so we dont have to wait for nvm to start

let g:coc_node_path='/home/nathan/.nvm/versions/node/v18.2.0/bin/node'

au BufRead,BufNewFile *.py setlocal ruler
au BufRead,BufNewFile *.py setlocal listchars=tab:\|\ 
au BufRead,BufNewFile *.py setlocal list
filetype plugin on

" -------
" configs for the zettel system
" autocompletion
au BufRead,BufNewFile *.md setlocal dictionary+=~/.vim/words/markdown.txt
au BufRead,BufNewFile *.md setlocal nonumber
au BufRead,BufNewFile *.md setlocal tw=63
au BufWritePre *.md call UpdateTags()

function! s:insertLink(content)
  let trimmed = substitute(a:content, '^\s\+', '', '')
  exe "normal! a(./" . trimmed . ")\<Esc>"
endfunction 
function! InsertLink()
  let finder = 'find ~/main/personal/thoughts -name "*.md" -not \( -path "*/node_modules/*" -o  -path "*/renv/*" \) -prune'
  let filter = 'sed -e "s/\/home\/nathan\/main\/personal\/thoughts\///"'
  let cmd = finder . '|' . filter
  let file = expand('%:p')
  let spec = {'source': cmd, 
        \'sink': function('s:insertLink')}
  call fzf#vim#files('~/main/personal/thoughts', fzf#vim#with_preview(spec))
endfunction
command! -nargs=* -bang Link call InsertLink()
noremap <leader>l :Link<CR>

function! s:insertTag(content)
  exe "normal! a" . a:content . "\<Esc>"
endfunction 
function! InsertTag()
  let spec = {'source': 'cat ~/.vim/words/markdown.txt',
        \'options': '--multi',
        \'sink': function('s:insertTag')}
  call fzf#vim#files('~/main', spec)
endfunction
command! -nargs=* -bang Tag call InsertTag()
noremap <leader>t :Tag<CR>

function! UpdateTags()
  let res = system("node", "~/main/personal/utils/notes/gen-tags.js")
endfunction 
command! -nargs=* -bang RefreshTags call UpdateTags()
noremap <leader>T :RefreshTags <CR>

function! ViewTagged()
endfunction

function ViewLinks()
endfunction

command! -nargs=* -bang ViewNotes call fzf#vim#files('~/main/personal/thoughts')
noremap <leader>n :ViewNotes<CR>

function NewNote(title)
  let filepath = system("node ~/main/personal/utils/templates/note.js ".a:title)
  execute 'edit '.filepath
endfunction
command! -nargs=1 Note call NewNote(<f-args>)

function! ZoteroCite()
  " pick a format based on the filetype (customize at will)
  let format = &filetype =~ '.*tex' ? 'citep' : 'pandoc'
  let api_call = 'http://127.0.0.1:23119/better-bibtex/cayw?format='.format.'&brackets=1'
  let ref = system('curl -s '.shellescape(api_call))
  return ref
endfunction
" -------

"--- for tsx/jsx
augroup FiletypeGroup
    autocmd!
    au BufNewFile,BufRead *.jsx set filetype=javascript.jsx
augroup END
augroup FiletypeGroup
    autocmd!
    au BufNewFile,BufRead *.tsx set filetype=typescript.tsx
augroup END

" --- for fcitx
" taken with minor adjustment from 
" https://wiki.archlinux.org/title/Fcitx#Shortcut_keys
let g:insert_lang = 'en'
function! OnNormal()
   let s:input_status = system("fcitx-remote")
   " if current language is kr, save that (otherwise save english) and 
   " switch to english
   if s:input_status == 2
      let g:insert_lang = 'kr'
    else
      let g:insert_lang = 'en'
   endif
  let l:a = system("fcitx-remote -c") " switch to english
endfunction

function! OnInsert()
   let s:input_status = system("fcitx-remote")
   " if current language is english and should be inesrting in korean,
   " switch to korean
   if s:input_status != 2 && g:insert_lang == 'kr'
      let l:a = system("fcitx-remote -o") " switch to kr
   endif
endfunction

set ttimeoutlen=150
"Exit insert mode
autocmd InsertLeave * call OnNormal()
"Enter insert mode
autocmd InsertEnter * call OnInsert()
"##### auto fcitx end ######

" true colors
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
let g:svelte_preprocessor_tags = [
  \ { 'name': 'ts', 'tag': 'script', 'as': 'typescript' }
  \ ]
let g:svelte_preprocessors = ['ts']

set termguicolors
set t_Co=256

source ~/.config/nvim/day-night.vim
source ~/.config/nvim/ayu.vim

hi VertSplit guifg=black
hi StatusLineNC guibg=#212733 guifg=#5CCFE6
hi StatusLine guibg=#212733 guifg=#FFFFFF

let g:ale_sign_column_always = 1
let g:ale_linter_aliases = {'svelte': ['css', 'javascript'], 
      \'vue': ['vue', 'javascript']}
let g:ale_linters = {'svelte': ['eslint'],
      \'vue': ['eslint'],
      \'typescript': ['eslint'],
      \'tsx': ['eslint'],
      \'jsx': ['eslint']}
let g:ale_fixers = {
      \'css': ['prettier'],
      \'yaml': ['prettier'],
      \'svelte': ['eslint', 'prettier'],
      \'vue': ['eslint', 'prettier'],
        \'javascript': ['prettier', 'eslint'],
        \'json': ['prettier'],
        \'typescript': ['prettier', 'eslint'],
        \'markdown': ['prettier'],
        \'html': ['prettier'],
        \}
let g:ale_fix_on_save = 1
let g:ale_lint_on_save = 1
let g:rustfmt_autosave = 1
let g:ale_lint_delay = 0

let g:fzf_colors = { 'fg': ['fg', 'Normal'],
      \ 'bg': ['bg', 'Normal'], 
      \ 'hl': ['fg', 'Comment'],
      \ 'fg+': ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
      \ 'bg+': ['bg', 'CursorLine', 'CursorColumn'],
      \ 'hl+': ['fg', 'Statement'],
      \ 'info': ['fg', 'PreProc'],
      \ 'border': ['fg', 'Ignore'],
      \ 'prompt': ['fg', 'Conditional'],
      \ 'pointer': ['fg', 'Exception'],
      \ 'marker': ['fg', 'Keyword'],
      \ 'spinner': ['fg', 'Label'],
      \ 'header':  ['fg', 'Comment'] } 

" remaps
" split management
nnoremap <C-w>s <C-w>s
nnoremap <C-w>e <C-w>v
nnoremap <C-w><C-e> <C-w>v
nmap <silent> <c-k> :wincmd k<CR>
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-h> :wincmd h<CR>
nmap <silent> <c-l> :wincmd l<CR>
noremap <leader>z "=ZoteroCite()<CR>p
nnoremap ; :
map <leader>s :Format<CR>
nnoremap <C-p> :Files<CR>
nnoremap <silent> <Leader>f :Rg<CR>
nnoremap <C-n> :set number!<CR>
nnoremap <Leader>gy :Goyo<CR>
tmap <S-Space> <Space>

function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction
nnoremap <silent> K :call ShowDocumentation()<CR>

nmap <silent> gy <Plug>(coc-type-definition)

" ctrl+c in visual mode becomes copy to system clipboard
vnoremap <C-c> "*y
" remember za is toggle fold

" misc
" changes directory to git root, useful for fzf
let gitroot = system('git rev-parse --show-toplevel 2> /dev/null | tr -d "\\n"')

if gitroot != ''
    execute 'chdir' fnameescape(gitroot)
  endif
