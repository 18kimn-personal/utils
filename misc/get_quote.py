from urllib.request import Request, urlopen
import json
from textwrap import wrap

def get_quote():

    req = Request('http://localhost:4100/')
    quote = json.load(urlopen(req))

    wrapped_body = "\n> ".join(wrap(quote.get("body"), width = 75))
    wrapped_attribution = "\n  ".join(wrap(quote.get("attribution")))

    return f"""> {wrapped_body}
>
> [{wrapped_attribution}]({quote.get("link")})"""
