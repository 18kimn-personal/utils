import sqlite3 from 'sqlite3'
// the following are node builtins
import {promises as fs} from 'fs'
import {dirname} from 'path'
import {fileURLToPath} from 'url'
import {homedir} from 'os'

const __dirname = dirname(fileURLToPath(import.meta.url))

/** promisified wrapper for sqlite3.Database.all */
async function getAll(db, query) {
  return new Promise((resolve) => {
    db.all(query, (_, rows) => resolve(rows))
  })
}

/** Given path of the zotero.sqlite database and 
 * a path to write to, parses and merges all tables from the 
 * sqlite file into a single JSON
 */
async function make_json(
  readPath, 
  writePath = `${__dirname}/zotero.json`
  ) {
  const db = new sqlite3.Database(readPath, sqlite3.OPEN_READONLY)

  const names = await getAll(db,
    "SELECT name FROM sqlite_master WHERE type='table';",
  )

  const zotero = await names.reduce(async (prev, curr) => {
    const table = await getAll(db, `SELECT * FROM ${curr.name}`)
    return {...(await prev), [curr.name]: table}
  }, {})

  fs.writeFile(writePath, 
    JSON.stringify(zotero, null, 1))
}

export default make_json

// Usage (you may need to alter this if you moved your data directory elsewhere)
make_json(`${homedir()}/Zotero/zotero.sqlite`)
