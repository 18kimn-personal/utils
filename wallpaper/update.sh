#! /bin/zsh

export DISPLAY=:0

NVM_DIR="$HOME/.nvm"
\. "$NVM_DIR/nvm.sh"  # This loads nvm

cd "$(dirname "$0")"

convert=/usr/local/bin/convert
basefont="/usr/share/fonts/source-pro/SourceSansPro-Regular.ttf"
boldfont="/usr/share/fonts/source-pro/SourceSansPro-Bold.ttf"
date=$(/usr/bin/env node ./date.js)
format="text 150,320 '$date'"

# picking random base wallpaper
n=$(ls base | wc -l)
i=$[ $RANDOM % $n + 1]
filename=$(ls base | head -n $i | tail -n 1)

convert "base/$filename" \
  -resize 1600x900! \
  -fill "rgba(0,0,0,0.6)" \
  -draw "rectangle 0, 0, 1600, 900" \
  products/shaded.png

convert products/shaded.png \
  -font $boldfont \
  -pointsize 48 -fill white \
  -draw "$format" \
  products/withDate.png

output=$(/usr/bin/env node ./quote.js)

convert products/withDate.png \
  -font $basefont \
  -fill "#FFFFFF" \
  -strokewidth 1 \
  \( -size 630x310 -background none \
  label:"$output" -trim -gravity center \
  -extent 630x310 \) \
  -gravity northwest \
  -geometry +135+400 \
  -composite products/finished.png

DISPLAY=:0 feh --bg-fill products/finished.png
