function wordWrap(str, cols = 45) {
  var formattedString = '',
      wordsArray = [];


  wordsArray = str.split(' ');

  formattedString = wordsArray[0];

  for (var i = 1; i < wordsArray.length; i++) {
      if (wordsArray[i].length > cols) {
          formattedString += '\n' + wordsArray[i];
      } else {
          if (formattedString.length + wordsArray[i].length > cols) {
              formattedString += '\n' + wordsArray[i];
          } else {
              formattedString += ' ' + wordsArray[i];
          }
      }
  }
  return formattedString
}

export default wordWrap