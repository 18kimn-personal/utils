import fetch from "node-fetch"
import { dirname } from "path"
import { fileURLToPath } from "url"

const __dirname = dirname(fileURLToPath(import.meta.url))
process.chdir(__dirname)

const wrap = (s) =>
  s.replace(/(?![^\n]{1,70}$)([^\n]{1,70})\s/g, "$1\n")

let shouldFetch = true
let quote = ""
while (shouldFetch) {
  quote = await fetch("http://localhost:4100").then((res) =>
    res.text()
  )
  shouldFetch = quote.length > 750
}

const { body, attribution } = JSON.parse(quote)
const formatted = `${wrap(body)}\n\n${wrap(attribution)}`

console.log(formatted)
