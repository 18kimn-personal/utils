const opts = {
  weekday: 'long',
  month: 'long',
  day: 'numeric',
}
const now = new Date().toLocaleString('en-US', opts)

console.log(now)
