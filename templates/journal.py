import os
import subprocess
from markdown.extensions import Extension
from os.path import exists, dirname
from os import makedirs
from datetime import date
from datetime import timedelta
from recently_played import get_recently_played
import pystache

import sys
sys.path.append('../misc')
from rollover_todos import rollover
from get_quote import get_quote

journal_path =  "/home/nathan/main/personal/decrypted/journal/daily/"

def format_date(diff):
    d = date.today() + timedelta(diff)
    pad = "0" if len(str(d.day)) == 1 else ""
    return f"{d.year}/{d.month}/{pad}{d.day}"
    
# sometimes i miss a day, so this just keeps going back as needed 
# to get the last set of tasks and to point the new entry to the right
# place.
def get_last_entry(diff = -1, repeats = 0):
    if repeats > 10: return format_date(-1)
    formatted = format_date(diff)
    filename = f"{journal_path}{formatted}.md"
    if exists(filename):
        return formatted
    else: return get_last_entry(diff - 1, repeats + 1)

def make_entry():
    today, tomorrow = map(format_date, (0, 1)) 
    yesterday = get_last_entry()
    entry = f"{journal_path}{today}.md"
    
    # we always want the name of the journal to be in STDOUT
    # so that i3 shortcuts can go to it quickly, so we print something
    # but if it already exists we dont need to do anything further,
    # just bring me to the journal file, so an early return is used
    print(entry)
    if exists(entry): return
    if not exists(dirname(entry)): makedirs(dirname(entry))

    previous_tasks = rollover(f"{journal_path}{yesterday}.md")
    template_file = open(dirname(__file__) + "/journal.md", "r")
    template = template_file.read()
    template_file.close()

    # things i wrote on a given day
    command = 'cd ~/main/personal/thoughts && git diff --compact-summary | head -n -1'
    notes = subprocess.check_output(command, shell = True).decode()
    # back up the notes directory after we get those changes
    command = '/home/nathan/main/personal/utils/bin/backup-notes'
    subprocess.Popen([command], shell = False, 
            stdout = subprocess.DEVNULL)

    filled = pystache.render(
            template,
            {'today': today,
            'yesterday': yesterday,
            'tomorrow': tomorrow,
            'todo': previous_tasks.get('normal'),
            'ongoing': previous_tasks.get('ongoing'),
            'listening': get_recently_played(),
            'quote': get_quote(),
            'notes': notes
            })

    entry_file = open(entry, "w")
    entry_file.write(filled)
    entry_file.close()

make_entry()

