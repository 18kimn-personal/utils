---
date: {{{today}}}
---

[[{{{yesterday}}} | <-]] | [[{{{tomorrow}}} | ->]]

---

## what i am doing today

{{{todo}}}

---

### ongoing tasks

{{{ongoing}}}

---

## what i am thinking today

---

## how i am feeling today

---

## what i am writing today

{{{notes}}}
---

{{{quote}}}

---

## listening to

{{{listening}}}
