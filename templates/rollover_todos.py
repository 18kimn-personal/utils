from markdown.extensions import Extension
from markdown.treeprocessors import Treeprocessor
from markdown.blockprocessors import BlockProcessor
from markdown import markdown
from os.path import exists
import xml.etree.ElementTree as etree
from xml.etree.ElementTree import tostring
import re

def rollover(filename):

    if not exists(filename): return {'normal': '', 'ongoing': ''}

    file = open(filename, "r")
    text = file.read()
    file.close()

    original_blocks = []
    first = True # no idea what the correct way is to get the first block
    class AddOriginalText(BlockProcessor):
        def test(self, parent, block):
            nonlocal first
            if first:
                first = False
                return True
            return False
        def run(self, parent, blocks):
            for block_num, block in enumerate(blocks):
                original_blocks.append(block)
                print(block_num, ':', blocks[0])
                self.parser.parseBlocks(parent, blocks[0:1])
                blocks.pop(0)
            # print(original_blocks)
            return False
        # 5/24: got the blocks to be individually processed without
        # errors
        # each block is a paragraph
        # for tomorrow: use *only* the block processor and repeat all
        # logic below
        # rationale is that we're doing our own regexing and we're
        # rendering into markdown anyhow, so might as well just use it
        # first filter blocks using the first five characters '- [ ]'
        # and find where is checked and unchecked
        # '\n'.join(blocks) should be used after filtering

    # once the "ongoing tasks" section is entered, all tasks
    # should be rolled over no matter if they are unchecked or not
    # this is kind of a messy way to do things but idk how to get
    # this markdown engine to follow the logic i have otherwise
    ongoing_passed = False
    ongoing_tasks = []
    unchecked_normal_tasks = []
    class FindPerpetualTasks(Treeprocessor):
        def run(self, root): 
            nonlocal ongoing_passed

            for element in root.iter():

                # print(element.tag, ':', tostring(element), '\n')
                # must come before is_task check because the 
                # 'ongoing tasks' section is not a task itself
                is_ongoing = element.tag[0] == 'h' and \
                        element.text == 'ongoing tasks'
                ongoing_passed = ongoing_passed or is_ongoing

                is_task = element.text and \
                        element.tag == 'li' and \
                        re.search(r"\[[x|\s]\]", element.text)
                if not is_task: continue

                task = element.text
                is_unchecked = '[ ]' == task[0:3]
                task = re.sub(r'^\[x\]', '[ ]', task)

                if ongoing_passed:
                    ongoing_tasks.append("- " + task)
                
                if is_unchecked and not ongoing_passed:
                    unchecked_normal_tasks.append("- " + task)
                    
    class MdExtension(Extension):
        def extendMarkdown(self, md):
            # md.parser.blockprocessors.register(AddOriginalText(md.parser),
                    # 'add', 175)
            md.treeprocessors.register(FindPerpetualTasks(md),
                    'findongoingtasks', 15)

    markdown(text, extensions = [MdExtension()])

    return {'ongoing': '\n'.join(ongoing_tasks), 
            'normal': '\n'.join(list(dict.fromkeys(unchecked_normal_tasks)))}

# rollover('/home/nathan/main/personal/decrypted/journal/daily/2022/5/23.md')
