    
import subprocess 

def getdiff():
    command = 'cd ~/main/personal/thoughts && git diff --compact-summary | head -n -1'
    notes = subprocess.check_output(command, shell = True).decode()
    return notes
