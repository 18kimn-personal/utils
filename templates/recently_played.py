from dotenv import load_dotenv
import os
import requests
import random

load_dotenv()

# should return array of {track: string, artist: string}
def get_recently_played():
    response = requests.get('http://localhost:' + os.getenv("PORT") + 
            "/token"
            )
    token = response.text

    tracks = requests.get(
            'https://api.spotify.com/v1/me/player/recently-played?limit=50',
            headers={"Authorization": "Bearer " + token}
        ).json()


    def get_items(tracks):
        og_items = tracks["items"]
        seen = set()
        filtered_items = []
        for ele in og_items:
            if ele['track']['id'] not in seen:
                filtered_items.append(ele)
                seen.add(ele['track']['id'])
        return random.sample(filtered_items, 6)

    def print_track(item):
        track = item['track']
        artist = track['artists'][0]['name']
        return f"- _{track['name']}_ by **{artist}**"

    resp = map(print_track, get_items(tracks))
    fmted = "\n".join(list(resp))
    return fmted
