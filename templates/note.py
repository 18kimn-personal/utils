from os.path import exists, dirname, abspath, getctime
from datetime import date, datetime
import pystache
import re
import sys

def format_day(d):
    month_pad = '0' if len(str(d.month)) == 1 else ''
    day_pad = '0' if len(str(d.day)) == 1 else ''
    return f"{d.year}-{month_pad}{d.month}-{day_pad}{d.day}"

def has_yaml(text):
    search = re.finditer(r"---\n", text)
    count = 0
    matches = []

    while True:
        try:
            count += 1
            matches.append(next(search))
        except StopIteration:
            break
    return len(matches) >= 2 and matches[0].span()[0] == 0

def add_date(text, today): 
    if not 'modified: ' in text: return text

    split = text.split('---\n')
    meta = split.pop(1)
    
    first = meta.find('[')
    last = meta.find(']')

    dates = meta[first+1:last].split(',')
    dates = list(map(lambda str: str.strip(), dates))
    if not dates[len(dates) - 1] == today:
        dates.append(today)

    appended = meta[0:first+1] +\
            ', '.join(dates) +\
            meta[last:]

    return '---\n'.join(['', appended, ''.join(split)])

def make_note():
    entry = abspath(sys.argv[1])
    title = sys.argv[2] if len(sys.argv) >= 3 else ''

    filename = entry if re.search(r"\.md$", entry) else f"{entry}.md"

    today = date.today()
    creation_time = datetime.fromtimestamp(getctime(filename)) if \
            exists(filename) else today
    creation_time = format_day(creation_time)

    template_file = open(dirname(__file__) + '/note.md', "r")
    template = template_file.read()
    template_file.close()

    filled = pystache.render(template, {
        "today": creation_time,
        "title": title
    })

    if exists(filename):
        file = open(filename, "r")
        text = file.read()
        with_yaml = text if has_yaml(text) else filled + text
        modified = add_date(with_yaml, format_day(today))
        file.close()
        file = open(filename, "w")
        file.write(modified)
    else: 
        file = open(filename, "w")
        file.write(filled)
    file.close()

    return filename

print(make_note())
