import {basename} from 'path'
import {promises as fs} from 'fs'

/** Given a path to search, recursively searches for node_modules
 * directories and returns an array of matches
 */
async function findNodeModules(path) {
  if (basename(path) === 'node_modules') return path
  const stat = await fs.lstat(path)
  if (!stat.isDirectory()) return []

  const files = await fs.readdir(path)
  const potentialMatches = files.map((file) =>
    findNodeModules(`${path}/${file}`),
  )
  return (await Promise.all(potentialMatches))
    .filter((result) => result.length) // empty === not node modules
    .flat(1) // unnest arrays; this happens recursively
}

export default findNodeModules
