import {promises as fs} from 'fs'
import child_process from 'child_process'
import {promisify} from 'util'

// can't figure out how to use yarn api so just using cli
const RESET = '\x1b[0m'
const BOLD = '\x1b[1m'
const exec = promisify(child_process.exec)

async function autoclean(path) {
  const files = await fs.readdir(path)
  const hasYarnlock = files.includes('yarn.lock')
  const hasYarnclean = files.includes('.yarnclean')

  const command = `
  cd ${path}
  ${hasYarnlock ? '' : 'yarn'}
  ${hasYarnclean ? '' : 'yarn autoclean --init'}
  yarn autoclean --force
  `

  const {error, stdout} = await exec(command)
  console.log(`${BOLD}Cleaning ${path}...`)
  if (error) {
    console.log(`${BOLD}${error}`)
  }
  if (stdout) {
    console.log(`${RESET}${stdout}`)
  }

  // if it doesnt have a yarnclean to start with,
  // clean up so we don't leave anything behind
  if (!hasYarnclean) fs.unlink(`${path}/.yarnclean`)
  if (!hasYarnlock) fs.unlink(`${path}/yarn.lock`)
}

export default autoclean
