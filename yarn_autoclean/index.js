import find from './find.js'
import autoclean from './autoclean.js'
import {dirname} from 'path'

async function autocleanAll(path) {
  const dirs = await find(path)

  dirs.forEach((dir) => autoclean(dirname(dir)))
}

autocleanAll('/home/nathan/Documents/main')
